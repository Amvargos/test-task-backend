<?php

namespace App\Enums;

/**
 * Эквайринги
 *
 * @method static CLOUD_PAYMENTS
 * @method static BEST2PAY
 */
class AcquiringEnum extends BaseEnum
{
    const CLOUDPAYMENTS = 'CloudPayments';
    const BEST2PAY = 'Best2Pay';
}
